# Clean theme for Bludit

A super clean single column theme with nothing but basic html, css, and no javascript. It is best to have a few static pages for the navigation row.

## Compatible
- Bludit v3.x

## Author
- Soemarko
- Minor changes by Bob Mottram

## Screenshot
<img src="https://code.freedombone.net/bashrc/bludit-nojs-clean/raw/master/screenshot.png?raw=true" width="60%"/>
